<?php
class Administrador{
    public $id;
    public $nome;
    public $email;
    public $senha;

    // public function getId(){
    //     return $this->id;
    // }
    // public function setId($value){
    //     $this->id=$value;    
    // }

    // public  function getNome(){
    //     return $this->nome;
    // }
    // public  function setNome($value){
    //     $this->nome=$value;    
    // }

    // public  function getEmail(){
    //     return $this->email;
    // }
    // public  function setEmail($value){
    //     $this->email=$value;    
    // }

    // public  function getSenha(){
    //     return $this->senha;
    // }
    // public  function setSenha($value){
    //     $this->senha=$value;    
    // }

    public static function loadById($id_adm){
        $sql = new Sql(); 
        $results = $sql->select("SELECT * FROM administrador WHERE id = :id",
        array(":id"=>$id_adm));
        if (count($results)>0){
            return $results[0];
        }
    }

    public static function getList(){
        $sql = new Sql();
        return $sql->select("SELECT * FROM administrador ORDER BY nome");
    }

    public static function search($adm){
        $sql = new Sql();
        return $sql->select("SELECT * FROM administrador WHERE nome LIKE :nome",
                array(":nome"=>"%".$adm."%"));
    }

    public function login($email, $senha){
        $senhacript = md5($senha);
        $sql = new Sql();
        $results = $sql->select("SELECT * FROM administrador WHERE email = :email AND senha = :senha",
                array(":email"=>$email,
                     ":senha"=>$senhacript)); 
        if(count($results)>0){
            $this->setData($results[0]);
        }
    }

    public static function setData($data){
        $id= $data['id'];
        $nome = $data['nome'];
        $email= $data['email'];
        $senha= $data['senha'];
    }

    public function insert(){
        $sql = new Sql();
        $results = $sql->select("CALL sp_adm_insert(:nome, :email, :senha)",
            array(
                ":nome"=>$this->nome,
                ":email"=>$this->email,
                ":senha"=>$this->senha
            ));
        if(count($results)>0){
            //$this->setData($results[0]);
            return $results[0];
        }
    }

    public function update($_id,$_nome,$_senha){
        //$this->setNome($_nome);
        //$this->setSenha($_senha);
        $sql = new Sql();
        $sql->query("UPDATE administrador SET nome = :nome, senha = :senha WHERE id = :id",
            array(
                ":id"=>$_id,
                ":nome"=>$_nome,
                ":senha" => md5($_senha)
            ));
    }

    public function delete(){
        $sql = new Sql();
        $sql->query("DELETE FROM administrador WHERE id = :id", 
        array(":id"=>$this->id));
    }
    // criando métodos construtores no PHP
    public function __construct($nome="",$email="",$senha=""){
        $this->nome=$nome;
        $this->email=$email;
        $this->senha = $senha;
    }

    public function __toString(){
        return json_encode(array(
            "id"=>$this->id,    
            "nome"=>$this->nome,
            "email"=>$this->email,
            "senha"=>$this->senha
                ));
    }
    
}
?>